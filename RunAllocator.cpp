// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>
#include <sstream>

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    int sets;
    cin >> sets;
    cin.ignore(256, '\n');
    while (sets-- > 0) {
        cin.ignore(256, '\n');
        string temp;
        my_allocator<double, 1000> allocator;
        while (cin.peek() != '\n' && !cin.eof()) {
            getline(cin, temp);
            istringstream s(temp);
            int n;
            s >> n;
            if (n > 0)
                allocator.allocate(n);
            else if (n < 0) {
                my_allocator<double, 1000>::iterator itr = allocator.begin();
                int index = -n;
                if (*itr < 0)
                    --index;
                while (index > 0 && itr != allocator.end()) {
                    if (index != 0)
                        ++itr;
                    if (*itr < 0) {
                        --index;
                    }
                }
                if (index == 0)
                    allocator.deallocate(reinterpret_cast<double*>(&*itr + 1), *itr/sizeof(double));
            }
        }
        my_allocator<double, 1000>::iterator itr = allocator.begin();
        cout << *itr;
        while (itr != allocator.end()) {
            ++itr;
            cout << " " << *itr;
        }
        cout << endl;
    }
    return 0;
}
