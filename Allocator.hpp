// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * @return true if blocks are valid, false otherwise
     */
    bool valid () const {
        const_iterator i = begin();
        bool is_valid = true;
        while (i != end() && is_valid) {
            // if block is free, check adjacent blocks for free blocks
            if (*i > 0 && *(&*i + 2 + (*i > 0 ? *i : -*i)/4) > 0) {
                is_valid = false;
            }
            ++i;
        }
        return is_valid;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (&*lhs == &*rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            _p += 2 + (*_p / 4 < 0 ? -(*_p / 4) : *_p / 4);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            _p -= 2 + (*(_p-1) / 4 < 0 ? -(*(_p-1) / 4) : *(_p-1) / 4);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (&*lhs == &*rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            _p += 2 + (*_p / 4 < 0 ? -(*_p / 4) : *_p / 4);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            _p -= 2 + (*(_p-1) / 4 < 0 ? -(*(_p-1) / 4) : *(_p-1) / 4);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }
        (*this)[0] = N - 8;
        (*this)[N - 4] = N - 8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * @param n number of elements to allocate for
     * @return pointer to the first element
     */
    pointer allocate (size_type n) {
        if (n < 1) {
            throw std::bad_alloc();
        }
        iterator itr = begin();
        int bytes_needed = n * sizeof(T);
        bool last_sentinel = false, found = false, entire_block = false;
        // find first fit
        while (!last_sentinel && !found) {
            if (itr == end()) {
                last_sentinel = true;
            }
            if (*itr >= bytes_needed) {
                // allocate entire block
                if (*itr - bytes_needed < sizeof(T) + (2 * sizeof(int))) {
                    entire_block = true;
                }
                found = true;
            }
            else {
                ++itr;
            }
        }

        // if found a fit, allocate the block
        if (found) {
            int* ptr = &*itr;
            // allocate to entire block instead of splitting in two
            if (entire_block) {
                int size = *ptr;
                *ptr = -size;
                *(ptr + 1 + size/4) = -size;
            }
            // allocate space and split
            else {
                int new_size = *ptr - (bytes_needed + 8);
                *ptr = -bytes_needed;
                *(ptr + 1 + bytes_needed/4) = -bytes_needed;
                *(ptr + 2 + bytes_needed/4) = new_size;
                *(ptr + 3 + bytes_needed/4 + new_size/4) = new_size;
            }
            assert(valid());
            return reinterpret_cast<T*>(ptr + 1);
        }
        else {
            throw std::bad_alloc();
        }
        assert(valid());
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * @param p pointer to first element in the block
     * @param n number of elements
     */
    void deallocate (pointer p, size_type n) {
        iterator i(reinterpret_cast<int*>(p) - 1);
        if (*i > 0) {
            throw std::invalid_argument("Invalid argument");
        }
        // deallocate
        int* ptr = &*i;
        *ptr = -*ptr;
        *(ptr + 1 + *ptr/4) = *ptr;
        // check adjacent blocks to coalesce
        if (i != begin()) {
            if (*(ptr - 1) > 0) {
                --i;
                int lhs = *(ptr - 1);
                int rhs = *ptr;
                *ptr = 0;
                *(ptr - 1) = 0;
                *(ptr - 2 - lhs / 4) = lhs + rhs + 8;
                *(ptr + 1 + rhs / 4) = lhs + rhs + 8;
                ptr = &*i;
            }
        }
        if (i != end()) {
            if (*(ptr + 2 + *ptr / 4) > 0) {
                int lhs = *ptr;
                int rhs = *(ptr + 2 + *ptr / 4);
                *ptr = lhs + rhs + 8;
                *(ptr + 1 + lhs / 4) = 0;
                *(ptr + 2 + lhs / 4) = 0;
                *(ptr + 3 + lhs / 4 + rhs / 4) = lhs + rhs + 8;
            }
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        // handle negative sizes
        int size = (*this)[N - 4] > 0 ? (*this)[N - 4] : -(*this)[N - 4];
        iterator i(&(*this)[N - 8 - size]);
        return i;
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        // handle negative sizes
        int size = (*this)[N - 4] > 0 ? (*this)[N - 4] : -(*this)[N - 4];
        const_iterator i(&(*this)[N - 8 - size]);
        return i;
    }
};

#endif // Allocator_h
