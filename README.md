# CS371p: Object-Oriented Programming Allocator Repo

* Name: (your Full Name)
Christopher Chen
* EID: (your EID)
ccc4456
* GitLab ID: (your GitLab ID)
chrischen88
* HackerRank ID: (your HackerRank ID)
chrischen2191
* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)
768c23f44f2a4a3d5fbccb42de90fad146b5e026
* GitLab Pipelines: (link to your GitLab CI Pipeline)
https://gitlab.com/chrischen88/cs371p-allocator/pipelines
* Estimated completion time: (estimated time in hours, int or float)
12 hours
* Actual completion time: (actual time in hours, int or float)
20 hours
* Comments: (any additional comments you have)
None