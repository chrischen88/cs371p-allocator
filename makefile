.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -I/usr/local/include -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
else ifeq ($(shell uname -p), unknown)
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov
    VALGRIND      := valgrind
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
endif

FILES :=                                      \
    RunAllocator                              \
    TestAllocator

CFILES :=                                     \
    .gitignore                                \
    .gitlab-ci.yml                            \
    allocator-tests \
    allocator-tests/chrischen88-RunAllocator.in  \
    allocator-tests/chrischen88-RunAllocator.out \
    Allocator.log                             \
    ctd-check                                 \
    html

TFILES := `ls allocator-tests/*.in`

allocator-tests:
	git clone https://gitlab.com/gpdowning/cs371p-allocator-tests.git allocator-tests

allocator-tests/%:
	./RunAllocator < $@.in > RunAllocator.tmp
	-diff RunAllocator.tmp $@.out

html: Doxyfile Allocator.hpp
	$(DOXYGEN) Doxyfile

Allocator.log:
	git log > Allocator.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	$(DOXYGEN) -g

RunAllocator: Allocator.hpp RunAllocator.cpp
	-$(CPPCHECK) RunAllocator.cpp
	$(CXX) $(CXXFLAGS) RunAllocator.cpp -o RunAllocator

RunAllocator.cppx: RunAllocator
	./RunAllocator < RunAllocator.in > RunAllocator.tmp
	-diff RunAllocator.tmp RunAllocator.out

TestAllocator: Allocator.hpp TestAllocator.cpp
	-$(CPPCHECK) TestAllocator.cpp
	$(CXX) $(CXXFLAGS) TestAllocator.cpp -o TestAllocator $(LDFLAGS)

TestAllocator.cppx: TestAllocator
	$(VALGRIND) ./TestAllocator
	$(GCOV) -b TestAllocator.cpp | grep -A 5 "File '.*TestAllocator.cpp'"

allocator-tests/%: RunAllocator
	./RunAllocator < $@.in > RunAllocator.tmp
	diff RunAllocator.tmp $@.out

all: $(FILES)

check: $(CFILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunAllocator
	rm -f TestAllocator

config:
	git config -l

ctd-check:
	$(CHECKTESTDATA) RunAllocator.ctd RunAllocator.in

ctd-generate:
	$(CHECKTESTDATA) -g RunAllocator.ctd RunAllocator.tmp

docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

format:
	$(ASTYLE) Allocator.hpp
	$(ASTYLE) RunAllocator.cpp
	$(ASTYLE) TestAllocator.cpp

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p-allocator.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Allocator.hpp
	-git add Allocator.log
	-git add html
	git add makefile
	git add README.md
	git add RunAllocator.cpp
	git add RunAllocator.ctd
	git add RunAllocator.in
	git add RunAllocator.out
	git add TestAllocator.cpp
	git commit -m "another commit"
	git push
	git status

run: RunAllocator.cppx

scrub:
	make clean
	rm -f  Allocator.log
	rm -f  Doxyfile
	rm -rf collatz-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	make clean
	@pwd
	@rsync -r -t -u -v --delete            \
    --include "Allocator.hpp"              \
    --include "RunAllocator.cpp"           \
    --include "RunAllocator.ctd"           \
    --include "RunAllocator.in"            \
    --include "RunAllocator.out"           \
    --include "TestAllocator.cpp"          \
    --exclude "*"                          \
    ~/projects/cpp/allocator/ .
	@rsync -r -t -u -v --delete            \
    --include ".gitignore"                 \
    --include ".gitlab-ci.yml"             \
    --include "Allocator.hpp"              \
    --include "makefile"                   \
    --include "RunAllocator.cpp"           \
    --include "RunAllocator.ctd"           \
    --include "RunAllocator.in"            \
    --include "RunAllocator.out"           \
    --include "TestAllocator.cpp"          \
    --exclude "*"                          \
    . downing@$(CS):cs/git/cs371p-allocator/

test: TestAllocator.cppx

tests: allocator-tests RunAllocator
	for v in $(TFILES); do make $${v/.in/}; done

versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Darwin)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
